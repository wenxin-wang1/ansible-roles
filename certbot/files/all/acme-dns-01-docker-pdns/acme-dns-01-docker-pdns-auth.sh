#!/bin/bash

image=dkreg.lab.ivi2.org:5000/pdns-server:latest
etc_dir=/etc/pdns
data_dir=/tmp/acme-pdns
mkdir -p $data_dir/$etc_dir

CREATE_DOMAIN=_acme-challenge.$CERTBOT_DOMAIN
name=acme.$CERTBOT_DOMAIN

cat <<EOF >$data_dir/$etc_dir/pdns.conf
launch = bind
bind-config = $etc_dir/named.conf
EOF

cat <<EOF >$data_dir/$etc_dir/named.conf
zone "$CREATE_DOMAIN" {
    type master;
    file "$etc_dir/db.acme";
};
EOF

cat <<EOF >$data_dir/$etc_dir/db.acme
\$TTL  0
@    IN    SOA    ns.$CREATE_DOMAIN. i+ivi.wenxinwang.me. (
           20180101       ; Serial
              3600        ; Refresh
              3600        ; Retry
            2419200       ; Expire
             604800 )     ; Negative Cache TTL
@    IN    TXT     $CERTBOT_VALIDATION
EOF

sudo docker run -d --name $name --rm \
     -p 53:53 -p 53:53/udp \
     -v $data_dir/$etc_dir:$etc_dir \
     $image

sleep 30
