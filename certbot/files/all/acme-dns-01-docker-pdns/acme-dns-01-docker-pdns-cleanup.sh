#!/bin/bash

data_dir=/tmp/acme-pdns
name=acme.$CERTBOT_DOMAIN

sudo docker stop $name || :
sudo rm -rf $data_dir
